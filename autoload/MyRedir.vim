function! MyRedir#Run(cmd,...)
    if a:0
        let mods = a:1
    else
        let mods = ''
    endif
    silent redir => s:redir
    try
        silent! exe a:cmd
    finally
        redir END
    endtry
    if empty(s:redir)
        echo 'No output from: '. a:cmd
    else
        exe mods .' new'
        setlocal bufhidden=wipe
        setlocal buftype=nofile
        setlocal noswapfile
        setlocal buflisted
        call append(1,split(s:redir,"\<NL>"))
        silent 1delete
        let i = 1
        let suffix = ''
        while 1
            try
                exe 'silent file '.fnameescape('Output of: '..a:cmd..suffix)
                break
            catch
                let i += 1
                let suffix = ' #'..i
            endtry
        endwhile
    endif
endfunction
