if has('nvim')
    command! -nargs=1 -complete=command R call R(<q-args>, <bang>0)
    function! R(cmd, ...) abort
        let bang = !empty(get(a:000,0))
        redir @r
        try
            silent exe a:cmd
        finally
            redir end
            let @r = trim(@r, "\r\n")
            let buf = nvim_create_buf(0,1)
            call nvim_open_win(buf, 1, {
                \ 'relative':'editor',
                \ 'row':3,
                \ 'col':6,
                \ 'height':(&lines-6),
                \ 'width':(&columns-12)
            \ })
            call nvim_buf_set_lines(buf, 0, -1, 1, [':'..a:cmd] + split(@r,"\n"))
        endtry
        return @r
    endfunction
else
    command! -nargs=+ -complete=command MyRedir call s:MyRedir_(<q-args>)
    command! -nargs=+ -complete=command R call s:MyRedir_(<q-args>)
    function! s:MyRedir_(args)
        let [mods, cmd] = matchlist(a:args, '\v^\s*(%(-[a-z]+\s+)*)(.*)$')[1:2]
        let mods = substitute(mods, '\v^\s+|-|\s+$', '', 'g')
        try
            if mods == ''
                call MyRedir#Run(cmd)
            else
                call MyRedir#Run(cmd, mods)
            endif
        endtry
    endfunction
endif
